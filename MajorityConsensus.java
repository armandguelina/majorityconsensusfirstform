/*------------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------IMPORTS---------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------*/

import java.io.*;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;  
import java.lang.Math;


/*------------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------Class-----------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------*/

public class MajorityConsensus  
{  

	//Fonction qui calcul les écarts types des notes des candidats
	public static String[][] ecartType(String[][] tabString, String file) throws IOException 
	{
		//Initialisation des variables
		String[][] tabVal    = new String[count(file)][2];
		ArrayList<String> al = new ArrayList<String>();
		double mediane       = 50;
		double total         = 0 ;
		int    current       = 0 ;
		int    cpt           = 0 ;
		String strTotal      = "";
		
		al.add("add"); //Ajout pour ocuuper le première place
		for(int i=1; i< tabString.length; i++)
		{
			//System.out.println(tab[i]);
			//Boucle permettant de dterminer l'indice correspond à la mediane poue chaque candidat
			for(int j=1; j<7; j++)
			{
				if(total >=mediane)
				{
					current  = j-1 ; 
					break;
				}
				else
				{
					total    = total + Double.parseDouble(tabString[i][j]);
				}
			}
			 
			total = 0;
			
			//Boucle qui permet de calculer l'écart type de chaque candidat par rapport à la médiane
			for(int j=1; j<7; j++)
			{
				total = total + ( ((Double.parseDouble(tabString[i][j]))*(Double.parseDouble(tabString[i][j]))) - (2*((Double.parseDouble(tabString[i][j]))*(Double.parseDouble(tabString[i][current])))) + ((Double.parseDouble(tabString[i][current]))*(Double.parseDouble(tabString[i][current]))) );
			}
			total = total/6;
			total = Math.sqrt(total);
			total = Math.round(total * 100.0) / 100.0;
			strTotal = total+"";
			al.add(strTotal);
			
			//Pour chaque candadidat, on inscrie son nom dans le tableau
			for(int k=0; k< tabVal.length; k++)
			{
				//System.out.println(tab[i]);
				for(int j=0; j<2; j++)
				{
					if(j==0)
					{
						tabVal[k][0] = tabString[k][0];
					}
					tabVal[0][1] = "Ecart type";
				}
			}
			
			total   = 0;
			current = 0;
			
		}
		
		//Ajout des valeurs des écarts types correspondant à chaque candidat dans le tableau
		for(int i =1; i< al.size(); i++)
		{
			tabVal[i][1] = al.get(i);
		}
		System.out.println(printResultsTab(tabVal, file));
		return tabVal;
	}

	//Fonction qui permet de compter le nombre de lignes dqns un fichier .csv
	//Prends en paramètres le nom du fichier et renvoie une nombre de lignes
	public static int count(String filename) throws IOException 
	{
		InputStream is = new BufferedInputStream(new FileInputStream(filename)); //Ouvre le fichier
		try 
		{
			//Initiation des variables
			byte[] c = new byte[1024];
			int count = 0;
			int readChars = 0;
			boolean empty = true;
			
			//Verifie à chaque fois si le ligne courrante est vide ou pas en utilisant la valeur du premier caractère trouvé
			while ((readChars = is.read(c)) != -1) 
			{
				empty = false;
				for (int i = 0; i < readChars; i++)
				{
					//Lorsqu'on arrive au dernier caractère de la ligne, avant le saut de ligne, nous incrementons le compteur de +1
				    if (c[i] == '\n') 
				    {
				        count++;
				    }
				}
			}
			//Si le compteur où le compter est de 0 ou fichier vide renvoie 0
			//Sinon renvoie le nombre de ligne du fichier
			return (count == 0 && !empty) ? 0 : count; 
		} 
		finally 
		{
			is.close();
	   }
	}
	
	
	//Fonction qui permet de lire un fichier .csv et de mettre le comtenu du fichier dans un tableau
	//Prend en paramètre le nom du fichier et renvoie un tableau contenant les valeurs du fichier
	public static String[][] readCSV(String file) throws IOException 
	{
		//Initialisation des variables
		String[] tab         = new String[count(file)];
		String[][] tabVal    = new String[count(file)][7];
		String val           = "";
		int cpt              = 0;
		//Analyse d'un fichier CSV dans le constructeur de la classe Scanner 
		Scanner sc           = new Scanner(new File(file));  
		sc.useDelimiter("\n");   //Delimiter  
		while (sc.hasNext())  //verification 
		{  
			val = sc.next();
			tab[cpt] = val; //Enregistre les valeurs de la ligne dans un tableau
			cpt++;
		}   
		sc.close();  //Fermeture de l'analyseur  
		
		//Boucle qui recupère les valeur de chaque ligne et stock le tout dans un tableau à 2 dimensions
		for(int i=0; i< tab.length; i++)
		{
			String[] tmp = tab[i].split(",");
			for(int j=0; j<7; j++)
			{
				tabVal[i][j] = tmp[j];
			}
		}
		System.out.println(print(tabVal, file)); //Visualisation avant renvoie
		return tabVal;

	}  
	
	//Fonction qui permet d'effectuer un jugement consensuelle
	public static void MajorityConsensus(String file) throws IOException 
	{
		//Initialisation des variables
		double[][] tabValDouble = new double[count(file)][7];
		String[][] tabVal       = new String[count(file)][7];
		String[][] tabValResults= new String[count(file)][2];
		String[][] tabValEcart  = new String[count(file)][2];
		
		tabVal       = readCSV(file); //Lie le fichier .csv
		tabValDouble = GetPercentages(tabVal, file); //Renvoie les pourcentages des votes
		System.out.println(printDouble(tabValDouble, file)); //Visualisation
		tabValResults= getResultsAccordingToMediane(tabValDouble, tabVal, file); //Resultat par rapport à la médiane
		tabValEcart = ecartType(tabVal, file); //Ecrat type de chaque candidat

		tabVal = order(tabValResults, tabValEcart, file);//Affiche les resultat par order du 1er au dernier
		System.out.println(printResultsTab(tabVal, file)); //Visualisation
		saveFile(tabVal); //Sauvegarde le tout dans un fichier
		System.out.println("File containing results generated : Results.tsv");
	}
	
	// function to sort hashmap by values
    public static HashMap<String, Integer> sortByValue(HashMap<String, Integer> hm)
    {
        // Create a list from elements of HashMap
        List<Map.Entry<String, Integer> > list =
               new LinkedList<Map.Entry<String, Integer> >(hm.entrySet());
 
        // Sort the list
        Collections.sort(list, new Comparator<Map.Entry<String, Integer> >() {
            public int compare(Map.Entry<String, Integer> o1,
                               Map.Entry<String, Integer> o2)
            {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });
         
        // put data from sorted list to hashmap
        HashMap<String, Integer> temp = new LinkedHashMap<String, Integer>();
        for (Map.Entry<String, Integer> aa : list) {
            temp.put(aa.getKey(), aa.getValue());
        }
        return temp;
    }
 


	//Fonction qui classe les candidats par rapport à l'écrat type
	//Prend en paramètre les tableaux 1. de classement avec égalités, 2. d'écrat type ainsi que le nom du fichier .csv
	public static String[][] order(String[][] tabString, String[][] tab, String file) throws IOException 
	{
	
		//Initialisation des variables
		HashMap<Integer, String> hm   = new HashMap<Integer, String>() ;
		HashMap<String, String> hmF   = new HashMap<String, String>()  ;
		HashMap<String, Integer> sort = new HashMap<String, Integer>() ;
		String[][] tabVal             = new String[count(file)][2];
		String[][] tabValEcart        = new String[count(file)][2];
		String[][] tabTmp             = new String[count(file)][2];
		String tmpGrade               = ""; 
		String tmpGradeE              = ""; 
		
		//Hashmap qui contient les valeurs numéric des différents notes afin de pouvoir de structurer le classement du 1er au dernier
		hm.put(1, "Reject");
		hm.put(2, "Poor");
		hm.put(3, "Acceptable");
		hm.put(4, "Good");
		hm.put(5, "VeryGood");
		hm.put(6, "Excellent");
		//System.out.println(hm);
		
		
			
		for (Map.Entry<Integer, String> set :hm.entrySet()) 
		{		
			for(int i = 1; i< tabString.length; i++)
        	{		
				if(tabString[i][1].equals(set.getValue()))
				{
					tabString[i][1] = set.getKey()+ "";
					sort.put(tabString[i][0], set.getKey());
				}
			}			
		}
		
		System.out.println(printResultsTab(tabString, file));
		System.out.println(sort);
		sort = sortByValue(sort);
		System.out.println(sort);

		
		for (Map.Entry<String, Integer> set :sort.entrySet()) 
		{		
			for (Map.Entry<Integer, String> set2 :hm.entrySet()) 
			{ 
				if(set.getValue() == set2.getKey())
				{
					hmF.put(set.getKey(), set2.getValue());
				}
			}			
		}
		
		System.out.println(hmF);
		
		for (Map.Entry<String, String> set :hmF.entrySet()) 
		{		
			for (int i = 1; i< tab.length; i++)
			{
					if(tab[i][0].equals(set.getKey()))
					{
						tabValEcart[i][0] = set.getKey();
						tabValEcart[i][1] = tab[i][1];
					}
			}			
		}
			
		System.out.println(printResultsTab(tabValEcart, file));
		String[] key = hmF.keySet().toArray(new String[0]);
		System.out.println(Arrays.toString(key));
		for (int i = 1; i< tab.length; i++)
		{	
			tabValEcart[i][0] = key[i-1];
			tabTmp[i][0]      = key[i-1];
		}	
		
		System.out.println(printResultsTab(tabValEcart, file));	

		System.out.println(printResultsTab(tabTmp, file));	
		
		for (int i = 1; i< tabValEcart.length; i++)
		{
			for (int j = 1; j< tab.length; j++)
			{	
				if(tabValEcart[i][0].equals(tab[j][0]))
				{
					tabValEcart[i][1] = tab[j][1];
				}
			}
		}	
		
		System.out.println(printResultsTab(tabValEcart, file)+ "fdghjkl");	
		
		for (int i = 1; i< tabTmp.length; i++)
		{
			for(Map.Entry<String, String> set :hmF.entrySet()) 
			{	
				if(tabTmp[i][0].equals(set.getKey()))
				{
					tabTmp[i][1] = set.getValue(); 
				}
			}
		}	

		System.out.println(printResultsTab(tabTmp, file));	
		
		for (int i = 1; i< tabTmp.length; i++)
		{
			if(	(i<(tabTmp.length-1)) && (tabTmp[i][1].equals(tabTmp[i+1][1])) )
			{
				System.out.println(tabValEcart[i+1][1]);	
				if(Double.parseDouble(tabValEcart[i][1]) > Double.parseDouble(tabValEcart[i+1][1]))
				{
					tmpGrade       = tabTmp[i][0];
					tabTmp[i][0]   = tabTmp[i+1][0];
					tabTmp[i+1][0] = tmpGrade;
					
					tmpGradeE       = tabValEcart[i][1];
					tmpGrade        = tabValEcart[i][0];
					tabValEcart[i][1]   = tabValEcart[i+1][1];
					tabValEcart[i][0]   = tabValEcart[i+1][0];
					tabValEcart[i+1][1] = tmpGradeE;
					tabValEcart[i+1][0]  = tmpGrade ;
					
				}
			}
		}
		
		tabTmp[0][0] = "Candidates";
		tabTmp[0][1] = "Grades";
		System.out.println(printResultsTab(tabTmp, file));	
		System.out.println(printResultsTab(tabValEcart, file));	
		tabVal = tabTmp;

		return tabVal;
	}
	
	
	//Fonction qui permet d'obetnir les résultats des candidats
	public static String[][] getResultsAccordingToMediane(double[][] tab, String[][] tabString, String file) throws IOException 
	{
		//Initialisation des variables
		String[][] tabVal    = new String[count(file)][2];
		ArrayList<String> al = new ArrayList<String>();
		double mediane       = 50;
		double total         = 0 ;
		int    current       = 0 ;
		
		al.add("add"); //Ajout pour ocuuper le première place
		
		//On determine l'indice de la mediane pour chaque candidat
		for(int i=1; i< tab.length; i++)
		{
			//System.out.println(tab[i]);
			for(int j=1; j<7; j++)
			{
				if(total >=mediane)
				{
					current  = j-1 ; 
					break;
				}
				else
				{
					total    = total + tab[i][j];
				}
			}
			total= 0;
			
			//System.out.println(total + " " + current);
			al.add(tabString[0][current]); //Ajout de la note dans le tableau de notes
			
			//Pour chaque candadidat, on inscrie son nom dans le tableau
			for(int k=0; k< tabVal.length; k++)
			{
				//System.out.println(tab[i]);
				for(int j=0; j<2; j++)
				{
					if(j==0)
					{
						tabVal[k][0] = tabString[k][0];
					}
					tabVal[0][1] = "grade";
				}
			}
			
			total   = 0;
			current = 0;
		}
		
		//Ajout des valeurs des notes correspondant à chaque candidat dans le tableau
		for(int i =1; i< al.size(); i++)
		{
			tabVal[i][1] = al.get(i);
		}
		System.out.println(printResultsTab(tabVal, file));
		return tabVal;
	}
	
	//Fonction qui permet d'obtenir les pourcentages correspondant aux notes des candidats
	public static double[][] GetPercentages(String[][] tab, String file) throws IOException 
	{
		//Initialisation des variables
		double[][] tabVal = new double[count(file)][7];
		double total      = 0;
		
		//Boucle qui pour chaque note donné à un candidat donné calcul son pourcentage
		for(int i=1; i< tab.length; i++)
		{
			//System.out.println(tab[i]);
			for(int j=1; j<7; j++)
			{
				total = total + Integer.parseInt(tab[i][j]);
			}
			//System.out.println(total);
			
			for(int j=1; j<7; j++)
			{
				double percent = (Double.parseDouble(tab[i][j]) / (double) total)*100;
				tabVal[i][j] = Math.round(percent * 100.0) / 100.0;
				//System.out.println(tabVal[i][j]);
			}
			total = 0;
		}
		return tabVal;
	}
	
	//Fontion permettant de sauvegarder le résultat du tout dans un fichier
	public static void saveFile(String[][] tab) throws IOException 
	{
		 // Creating an instance of file
        Path path = Paths.get("Results.tsv");

        // Custom string as an input
        String str = "";
            

		for ( int i = 0; i < tab.length; i++ )
		{
			for ( int j = 0; j < 2; j++ )
			{
				if(j ==1)
				{
					str = str + tab[i][j] + "\t" + i + "\n";
				}
				else
				{
					str = str + tab[i][j] + "\t";
				}
			}
		}
		
        Files.writeString(path, str, StandardCharsets.UTF_8);
	}
	
	
	/*------------------------------------------------------------------------------------------------------------------------*/
	/*--------------------------------------------FONCTIONS POUR AFFICHAGES---------------------------------------------------*/
	/*------------------------------------------------------------------------------------------------------------------------*/
	
	public static String printResultsTab(String[][] tab, String file) throws IOException 
	{
		String str = "";
		for( int k =0; k< 2; k++)
		{
			str= str + "+---" ;
		}
		str = str + "\n" ;

		for ( int i = 0; i < count(file); i++ )
		{
			for ( int j = 0; j < 2; j++ )
			{
				str = str + "|";
				str = str + " " + tab[i][j] + " ";
			}

			str = str + "|\n";

			for( int l =0; l< 2; l++)
			{
				str = str + "+---" ;
			}
			str = str + "\n" ;
		}
		return str;
	}
	
	
	public static String print(String[][] tab, String file) throws IOException 
	{
		String str = "";
		for( int k =0; k< 7; k++)
		{
			str= str + "+---" ;
		}
		str = str + "\n" ;

		for ( int i = 0; i < count(file); i++ )
		{
			for ( int j = 0; j < 7; j++ )
			{
				str = str + "|";
				str = str + " " + tab[i][j] + " ";
			}

			str = str + "|\n";

			for( int l =0; l< 7; l++)
			{
				str = str + "+---" ;
			}
			str = str + "\n" ;
		}
		return str;
	}
	
	public static String printDouble(double[][] tab, String file) throws IOException 
	{
		String str = "";
		for( int k =0; k< 7; k++)
		{
			str= str + "+---" ;
		}
		str = str + "\n" ;

		for ( int i = 0; i < count(file); i++ )
		{
			for ( int j = 0; j < 7; j++ )
			{
				str = str + "|";
				str = str + " " + tab[i][j] + " ";
			}

			str = str + "|\n";

			for( int l =0; l< 7; l++)
			{
				str = str + "+---" ;
			}
			str = str + "\n" ;
		}
		return str;
	}
	
	
	/*------------------------------------------------------------------------------------------------------------------------*/
	/*---------------------------------------------------------Main-----------------------------------------------------------*/
	/*------------------------------------------------------------------------------------------------------------------------*/
	
	public static void main(String[] args) throws Exception  
	{  
		System.out.println(count("MC.csv"));
		MajorityConsensus("MC.csv");
	}
	

}  
